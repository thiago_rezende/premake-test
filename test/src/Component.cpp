#include "Component.h"

Component::Component()
{
    OnAttach();
}

Component::~Component()
{
    OnDetach();
}