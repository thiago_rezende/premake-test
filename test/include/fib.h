#ifndef _FIB_H_
#define _FIB_H_
#include <iostream>
#include <vector>

int fib(std::vector<long>& memo, int n)
{
    if (memo[n] == -1)
        memo[n] = fib(memo, n - 1) + fib(memo, n - 2);
    else
        return memo[n];
    return memo[n];
}

#endif // _FIB_H_