#ifndef _COMPONENT_H_
#define _COMPONENT_H_

class Component
{
public:
    Component();
    ~Component();
    virtual void OnAttach();
    virtual void OnDetach();
public:
    const char* m_name;
};

#endif // _COMPONENT_H_