#ifndef _ENTITY_H_
#define _ENTITY_H_

#include <vector>
#include "Component.h"

class Entity
{
public:
    Entity();
    Entity(float n_x, float n_y, float n_z);
    ~Entity();
public:
    std::vector<Component> components;
};

#endif // _ENTITY_H_