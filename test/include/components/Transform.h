#ifndef _TRANSFORM_H_
#define _TRANSFORM_H_

#include "../Component.h"

class Transform : public Component
{
public:
    float x;
    float y;
    float z;
};

#endif // _TRANSFORM_H_