workspace "vscode-test"
    architecture "x64"
    configurations {"Release", "Debug"}
	startproject "test"
	location ".build/"
    

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

project "test"
    language "C++"
    kind "ConsoleApp"
    cppdialect "C++17"
	systemversion "latest"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("obj/" .. outputdir .. "/%{prj.name}")

    files
	{
		"%{prj.name}/include/**.h",
		"%{prj.name}/src/**.cpp"
	}

    includedirs
	{
		"%{prj.name}/include"
    }
    
    filter "configurations:Debug"
		defines "DEBUG"
		runtime "Debug"
		symbols "On"

	filter "configurations:Release"
		defines "NDEBUG"
		runtime "Release"
		optimize "On"